//
//  SoloEditorViewController.h
//  SoloFramework
//
//  Created by Pavel Yurchenko on 9/28/16.
//  Copyright © 2016 Tipit Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SoloEditorViewControllerDelegate;
@class GPUImageView;

/*!
    @class SoloEditorViewController
 
    @brief This is the default editor view controller
 
    @discussion This class implements functionality of Solo default editor. It should be used as a base class when creating custom editor.
 */

@interface SoloEditorViewController : UIViewController

/*!
    @brief gpuImageView outlet
 
    @discussion This one must be connected with the appropriate view in the storyboard to render the media that is being edited correctly on it
 */
@property (nonatomic, strong) IBOutlet GPUImageView *gpuImageView;


/*!
    @brief Delegate
 
    @discussion It's used to notify about editing finished or canceled events
 */
@property (nonatomic, weak) id<SoloEditorViewControllerDelegate> delegate;

/*!
    @brief Cancel button visibility token
 
    @discussion Use it to get or set Cancel button hidden state. Assign YES to hide Cancel button on default editor
 */
@property (nonatomic, assign) BOOL cancelButtonIsHidden;


/*!
    @brief Done button visibility token
 
    @discussion Use it to get or set Done button hidden state. Assign YES to hide Done button on default editor
 */
@property (nonatomic, assign) BOOL doneButtonIsHidden;


/*!
    @brief Custom Filters button visibility token
 
    @discussion Use it to get or set Custom Filters button hidden state. Assign YES to hide Custom Filters button on default editor
 */
@property (nonatomic, assign) BOOL customFiltersButtonIsHidden;


/*!
    @brief Music button visibility token
 
    @discussion Use it to get or set Music button hidden state. Assign YES to hide Music button on default editor
 */
@property (nonatomic, assign) BOOL musicButtonIsHidden;


/*!
    @brief Effects tool visibility token
 
    @discussion Use it to get or set Effects Tool hidden state. Assign YES to hide Effects Tool on default editor
 */
@property (nonatomic, assign) BOOL effectsToolIsHidden;


/*!
    @brief URL to media being edited
 */
@property (nonatomic, copy) NSURL *mediaURL;


/*!
    @brief First frame image
 
    @discussion First frame image extracted from current media. It can be used for generating effects thumbnails
 */
@property (nonatomic, readonly) UIImage *firstImageThumbnail;


/*!
    @brief Default editor static constructor
 
    @discussion The following static constructor is a MUST for each custom editor view controller class, it is used by the SoloManager code to create instance and present it.
                Create similar one if you are creating custom editor based on SoloEditorViewController for your successor class
 */
+ (SoloEditorViewController*)controllerFromStoryboard;


/*!
    @brief Cancel action
 
    @discussion Call to simulate back/cancel button press
 */
- (void)cancel;


/*!
    @brief Done action
 
    @discussion Call to simulate done button press. In this case the result will be saved and returned in callback
 */
- (void)done;


@end


/*!
    @protocol SoloEditorViewControllerDelegate
    
    @brief Default editor delegate protocol
 
    @discussion Implement it to receive notifications from editor
 */

@protocol SoloEditorViewControllerDelegate <NSObject>

/*!
    @brief Did cancel notification
 
    @discussion This method is called when user presses Cancel button on the editor or when <i>cancel</i> method of editor is fired in the code
 
    @param sender Reference to the editor view controller that called it
 */
- (void)onSoloEditorViewControllerDidCancel:(SoloEditorViewController*)sender;

/*!
    @brief Did finish notification
 
    @discussion This method is called when user presses Done button on the editor or when <i>done</i> method of editor is fired in the code and right after saving result media is finished
 
    @param sender Reference to the editor view controller that called it
    @param url URL to the result media
 */
- (void)onSoloEditorViewController:sender didFinishExport:url;

@end
