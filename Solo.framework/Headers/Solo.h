//
//  SoloFramework.h
//  SoloFramework
//
//  Created by Pavel Yurchenko on 8/4/16.
//  Copyright © 2016 Tipit Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SoloManager.h"
#import "SoloEditorViewController.h"

#import "IEffectsManager.h"
#import "IEffect.h"
#import "IBGManager.h"
#import "IBGVideo.h"
