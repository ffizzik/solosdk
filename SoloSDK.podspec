Pod::Spec.new do |s|
  s.name             = 'SoloSDK'
  s.version         = '0.0.2'
  s.platform     = :ios, '9.0'
  s.homepage         = 'http://www.tipit.tv'
  s.authors         = { 'Eugene' => 'ffizzik@gmail.com' }
  s.license         =  { :type => 'BSD' }
  s.summary         = 'A cocoa pod containing the Solo framework.'
  s.source           = { :git => 'https://ffizzik@bitbucket.org/ffizzik/solosdk.git', :tag => '0.0.2' }
  s.frameworks = 'Solo'
  s.vendored_frameworks = 'Solo.framework'
  s.xcconfig = { "OTHER_LDFLAGS" => "-lz" }

end
